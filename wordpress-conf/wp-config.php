<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress-user' );

/** MySQL database password */
define( 'DB_PASSWORD', 'a982lsnn' );

/** MySQL hostname */
define( 'DB_HOST', 'db' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Y*FVt(amM(nh1dt5}I+XSRP7aW&r0x4YMciz&8p]f@Py)HKc&au8|5|}q|Pb-:D@');
define('SECURE_AUTH_KEY',  'kDXqD75DXJu-0+CJ{:O|Lj+a<S90kLo{pd;b!Mqg@K{A;ADv k|NQmOu~(1DnGks');
define('LOGGED_IN_KEY',    '45GLY9m+X~Nwl(U25&!4#]-94@~(X8Hu2!?{m$A$H-`CGLC2l_xk (cAb0)pIaOG');
define('NONCE_KEY',        'ChlP3_hz]1d[@s|xj55WR?PJ!o?Si=I@m$6J9p}fSt)&!6OIz.sYz+:9/Dh=:b 8');
define('AUTH_SALT',        'ivtypvOL[>Tt.6so Ig~Y}~NKM/D9+pi/BmADN^{x-t2{EHT{?IdA a~^#Y4Gm*j');
define('SECURE_AUTH_SALT', 'MXlO[yPy/>Kt*w4K>A|2E];+-Y_r-&ZJ-%{R.z;s6D,Id3:8 T-qT4i=_Oev!%57');
define('LOGGED_IN_SALT',   'rD34e-^aiK.m0mL nc2$eo%H5Vn F6F^ap9>6rXkX@-8<]MF4!C9`CX!fagFpbDO');
define('NONCE_SALT',       'Y/a()P329EKv~N{k196(.7Y7VBZ4}=3io^?P(%1BSzDkD?scd!cBL*Kh:{Jas8]g');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
