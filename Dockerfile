FROM php:7.2-apache

RUN apt-get update && apt-get install -y \
        curl \
        wget \
        git \
    && docker-php-ext-install mysqli pdo_mysql

WORKDIR /var/www/html

COPY wordpress-conf/ /var/www/html
